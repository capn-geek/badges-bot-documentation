.. Discord Badges Bot documentation master file, created by
   sphinx-quickstart on Thu Jan 23 10:46:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Discord Badges Bot's documentation!
==============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   commands



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
