Cog File Documentation
======================

.. py:module:: command_basics
.. py:class:: general(bot)
   A discord.py commands cog that adds support for many general-use commands when loaded onto the bot object.
     
   :param bot: The bot object to bind to the cog
   :type bot: discord.ext.commands.Bot
     
   .. py:method:: ping(ctx)
      Ping the bot and send its current latency in milliseconds rounded to one decimal place.
        
      :param ctx: The invocation context of the command. This is included when the method is called automatically through command invocation.